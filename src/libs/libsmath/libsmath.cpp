#include <iostream>
#include <cmath>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <regex>
#include <locale>
#include <clocale>
#include "smath.h"

#define NDEBUG

//Definuje makro debug() pro debug vypis
#ifndef NDEBUG
#define debug(x) std::cerr << x << std::endl
#else
#define debug(x)
#endif

/**
 * @brief Vlastni facet pro locale, nastavi desetinny oddelovac jako tecku
 * a zakaze oddelovani tisicu
 * 
 */
class custom_numpunct : public std::numpunct<char> {
    protected:
        /**
         * @brief Zakaze oddelovac tisicu
         */
        virtual std::string do_grouping() const {
            return "\000";
        }
        
        /**
         * @brief Nastavi desetinny oddelovac jako tecku
         */
        virtual char do_decimal_point() const {
            return '.';
        }
};

/*
 * @brief Zjisti zda je v promenne typu double ulozena integer hodnota
 * @param d Testovana double hodnota
 * @return true/false pokud v promenne je/neni integer
 */
bool is_int(double d) {
    return floor(fabs(d)) == fabs(d);
}

/*
 * @brief Zjisti, zda je cele cislo sude nebo liche
 * @param i Testovane cislo
 * @return true/false pokud je cislo sude/liche 
 */
bool is_even(int i) {
    return i % 2 == 0;
}

/*
 * @brief Vymaze z retezce vsechny mezery
 * @param *str Ukazatel na retezec, se kterym bude funkce pracovat
 * @return Funkce nic nevraci (retezec se predava odkazem)
 */
void remove_spaces(std::string *str) {
    str->erase( std::remove_if(str->begin(), str->end(), ::isspace), str->end());
}

/*
 * @brief Nahradi vsechny znaky ',' v retezci za znaky '.'
 * @param *str Ukazatel na retezec, se kterym bude funkce pracovat
 * @return Funkce nic nevraci (retezec se predava odkazem)
 */
void remove_commas(std::string *str) {
    std::replace(str->begin(), str->end(), ',', '.');
}

/*
 * @brief Obal pro funkci stod() ktery nahradi desetinne carky za tecky
 * @param str Retezec pro nahrazeni
 * @param *sz Ukazatel na string::size_type pro funkci stod()
 * @return Retezec prevedeny na double
 */
double to_double(std::string str, std::string::size_type *sz) {
    remove_commas(&str);
    debug("To double (got): " << str);
    double d = std::stold(str, sz);
    debug("To double (ret): " << d);
    return d;
}

/*
 * @brief Pretizeny obal pro funkci stod() ktery nahradi desetinne carky za tecky
 * @param str Retezec pro nahrazeni
 * @return Retezec prevedeny na double
 */
double to_double(std::string str) {
    remove_commas(&str);
    debug("To double (got): " << str);
    double d = std::stold(str);
    debug("To double (ret): " << d);
    return d;
}

/*
 * @warning Pocita jen s celymi cisly! (i kdyz je argumentem a navratovou hodnotou double)
 * @brief Vyresi faktorial pro n
 * @param n Operand pro faktorial
 * @return Faktorial pro dane cislo jako double
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskou
 */
double fact(double n) {
    using namespace std;
    double res = 1;
    
    if (n < 0 || !is_int(n)) {
        throw std::runtime_error(ERR_FACT);
    }
    
    while (n) {
        res *= n--;
    }
    
    return res;
}

/*
 * @brief Spocita n-tou odmocninu z cisla x
 * @param n Kolikata odmocnina
 * @param x Cislo ze ktereho pocitame odmocninu
 * @return n-tou odmocninu z x jako double
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskou
 */
double root(int n, double x) {
    //Nepocita zaporne odmocniny
    if (n <= 0) {
        throw std::runtime_error(ERR_ROOT);
    }
    
    //Suda odmocnina ze zaporneho cisla neexistuje
    if (is_even(n) && x < 0) {
        throw std::runtime_error(ERR_ROOT);
    }
    
    return pow(x, 1.0 / n);
}

/*
 * @brief Spocita x modulo y
 * @param n Prvni operand
 * @param m Druhy operand
 * @return Vysledek jako integer
 */
int mod(int x, int y) {
    return (x % y + y) % y;
}

/**
 * @brief Vyresi jednoduchy vyraz s jednim operandem
 * @param expr Retezec obsahujici vyraz
 * @return Vysledek vyrazu jako double
 * @exception runtime_error Pri matematicke chybe (faktorial ze zap. cisla apod.)
 * @exception invalid_argument Pri neplatnem vyrazu expr
 */
double unary_solve(std::string expr) {
    using namespace std;
    
    string::size_type sz = 0;
    //Operand
    double num = 0.0;
    //Operator
    char op = 0;
    
    //Vymaze z expr mezery
    remove_spaces(&expr);
    
    //Parsuje expr
    try {
        //Operator je posledni
        num = to_double(expr, &sz);
        op = expr[sz];
    } catch (invalid_argument &e) {
        //Operator je prvni
        try {
            op = expr[0];
            num = to_double(expr.substr(1));
        } catch (invalid_argument &e) {
            //Chyba
            throw invalid_argument(ERR_INV);
        }
    }
    
    debug("Unary solve: num: " << num << " op: " << op);
    
    //Na zaklade operatoru provede a vrati operaci
    switch (op) {
        case '!':
            return fact(num);
        break;
        
        case 'r':
            return sqrt(num);
        break;
        
        default:
            throw invalid_argument("Neplatny operator: " + op);
        break;
    }
}

/**
 * @brief Vyresi jednoduchy vyraz o dvou operandech
 * @param expr Retezec obsahujici vyraz
 * @return Vysledek vyrazu jako double
 * @exception runtime_error Pri matematicke chybe (deleni nulou apod.)
 * @exception invalid_argument Pri neplatnem retezci expr
 */
double binary_solve(std::string expr) {
    using namespace std;
    string::size_type sz;
    
    //Prvni operand
    double num1 = 0.0;
    //Operator
    char op = 0;
    //Druhy operand
    double num2 = 0.0;
    
    //Vymaze z expr mezery
    remove_spaces(&expr);
    
    //Parsuje expr
    try {
        num1 = to_double(expr, &sz);
        op = expr[sz++];
        num2 = to_double(expr.substr(sz));
    } catch (invalid_argument &e) {
        //stod obdrzel neplatny argument
        throw invalid_argument(ERR_INV);
    } catch (out_of_range &e) {
        //expr neobsahuje druhy operand
        throw invalid_argument(ERR_INV);
    }
    
    
    debug("Binary solve: num1: " << num1 << " op: " << op << " num2: " << num2);
    
    //Na zaklade operatoru vykona a vrati operaci
    switch (op) {
        case '+':
            return num1 + num2;
        break;
        
        case '-':
            return num1 - num2;
        break;
        
        case '*':
            return num1 * num2;
        break;
        
        case '/':
            if (num2 != 0.0) {
                return num1 / num2;
            } else {
                throw runtime_error(ERR_ZERODIV);
            }
            
        break;
        
        case '%':
            if (num2 != 0.0) {
                if (is_int(num1) && is_int(num2)) {
                    return mod((int)num1, (int)num2);
                } else {
                    throw runtime_error(ERR_MOD);
                }
            } else {
                throw runtime_error(ERR_ZERODIV);
            }
        break;
        
        case '^':
            return pow(num1, num2);
        break;
        
        case 'r':
            if (is_int(num1)) {
                return root(num1, num2);
            } else {
                throw runtime_error(ERR_ROOT);
            }
        break;
        
        default:
            throw invalid_argument("Neplatny opeartor: " + op);
        break;
    }
}

/**
 * @brief Nahradi prvni vyraz odpovidajici rgx jeho vysledkem
 * @param *expr Ukazatel na retezec s vyrazem
 * @param rgx Regexp odpovidajici hledanemu vyrazu
 * @return true pokud byl odpovidajici vyraz nalezen, jinak false
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskou
 * @note K vypocteni vyrazu vyuziva funkce binary_solve() a unary_solve()
 */
bool rgx_solve(std::string *expr, std::regex rgx) {
    using namespace std;
    smatch m; //Obsahuje nalez z expr odpovidajici rgx
    
    //Najde prvni shodu s rgx
    regex_search(*expr, m, rgx);
    
    //Pokud neni nalezena shoda, vrati false
    if (m.str() == "") {
        return false;
    }
    
    debug(endl << "Solving: " << m.str());
    
    //Vyresi nalezenou shodu
    double res;
    try {
        //Zkusi retezec vyresit jako binarni operaci
        res = binary_solve(m.str());
    } catch (invalid_argument &e) {
        //Pokud binarni reseni selhalo, zkusi retezec vyresit jako unarni operaci
        debug("Binary solve: " << m.str() << " failed");
        
        try {
            res = unary_solve(m.str());
        } catch (invalid_argument &e) {
            //Binarni i unarni reseni selhalo, vyvola vyjimku
            throw runtime_error(ERR_INV);
        }
        
        debug("Unary solve: " << m.str() << " success");
    }
    
    //Vysledek prevede na retezec, i se znamenkem +/-
    string str_res = (res >= 0) ? '+'+to_string(res) : to_string(res);
    
    //Nahradi nalezeny vyraz jeho vysledkem
    expr->assign(regex_replace(*expr, rgx, str_res, regex_constants::format_first_only));
    
    debug("Expr: " << *expr);
    
    return true;
}


/**
 * @brief Vyresi slozity matematicky vyraz s podporovanymi operacemi
 * @warning Podporovane operace zahrnuji +, -, *, /, %, ! (faktorial), ^ (mocnina), r (odmocnina)
 * @warning Neumi pocitat se zavorkami! (vyvola vyjimku)
 * @param expr Retezec obsahujici vyraz. Vice o vstupnich retezcich @link rules zde @endlink
 * @return Vysledek vyrazu jako double
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskou
 * @note Pouziva funkci rgx_solve() pro vypocty vyrazu
 */
double solve_expr(std::string expr) {
    using namespace std;
    
    /*
     * Regulerni vyrazy odpovidajici jednotlivym vyrazum
     */
    
    //Odpovida vyrazum faktorial, napt 2!, 324!
    regex rgx_fact("([+\\-]?[0-9]+([.,][0-9]+)?!)");
    
    //Odpovida vyrazum umocneni a odmocneni, napr 2^4, -4.2^13.37, 3r27, r64
    regex rgx_exp("([+\\-]?[0-9]+([.,][0-9]+)?\\^[+\\-]?[0-9]+([.,][0-9]+)?)|(([0-9]+)?r\\+?[0-9]+(\\.[0-9]+)?)");
    
    //Odpovida vyrazum nasobeni a deleni (i modulo), napr -2.8*3, 3/-4.5 atd..
    regex rgx_muldiv("([+\\-]?[0-9]+([.,][0-9]+)?[*/%][+\\-]?[0-9]+([.,][0-9]+)?)");
    
    //Odpovida vyrazum scitani a odcitani
    regex rgx_addsub("([+\\-]?[0-9]+([.,][0-9]+)?[+\\-][+\\-]?[0-9]+([.,][0-9]+)?)");
    
    /*
     * Postupne vyresi vsechny vyrazy po urovnich jejich prednosti
     */
     
    //Odstrani mezery
    remove_spaces(&expr);
    
    //LEVEL1 - Vyresi faktorialy
    while(rgx_solve(&expr, rgx_fact));
    
    //LEVEL2 - Vyresi mocniny a odmocniny
    while(rgx_solve(&expr, rgx_exp));
    
    //LEVEL3 - Vyresi deleni, modulo a nasobeni
    while(rgx_solve(&expr, rgx_muldiv));
    
    //LEVEL4 - Vyresi scitani a odcitani
    while (rgx_solve(&expr, rgx_addsub));
    
    //Vyreseny retezec se pokusi prevest na cislo
    string::size_type sz;
    double res = 0.0;
    
    try {
        res = to_double(expr, &sz);
    } catch (invalid_argument &e) {
        //Pokud se nepovedlo, jedna se o neplatny vyraz
        throw runtime_error(ERR_INV);
    }
    
    //Retezec musi obsahovat jen jedno cislo udavajici vysledek
    if (expr.substr(sz) != "") {
        //Po vyjmuti cisla retezec nezustal prazdny, jedna se o neplatny vyraz
        throw runtime_error(ERR_INV);
    }
    
    return res;
}

/*
 * @brief Vyresi slozity matematicky vyraz s podporovanymi operacemi a zavorkami
 * @warning Podporovane operace zahrnuji +, -, *, /, %, ! (faktorial), ^ (mocnina), r (odmocnina)
 * @param expr Retezec obsahujici vyraz. Vice o vstupnich retezcich @link rules zde @endlink
 * @return Vysledek vyrazu jako double
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskouch
 */
double solve(std::string expr) {
    //Nastavi vlasni locale (desetinne tecky, zadny oddelovac tisicu)
    std::locale loc(std::locale(), new custom_numpunct());
    std::locale::global(loc);
    std::cerr.imbue(loc);
    
    setlocale(LC_NUMERIC, "C");
    
    using namespace std;
    
    //Integer, float, znaky '+-*/!^%' a znak 'r' oznacujici odmocninu
    regex rgx_valid("([+\\-]?[0-9]+([.,][0-9]+)?)|([+*/!r%^()\\-])");
    
    //Odpovida zavorkam, ve kterych nejsou jine zavorky
    regex rgx_brackets("(\\([^()]*\\))");
    
    //Odstrani mezery a nahradi carky za tecky
    remove_spaces(&expr);
    remove_commas(&expr);
    
    //Pokud se v retezci vyskytuje neco jineho nez by melo, vyvola vyjimku
    if (regex_replace(expr, rgx_valid, "") != "") {
        throw runtime_error(ERR_INV);
    }
    
    debug("Parsing: " << expr);
    
    //Vyresi vsechny zavorky
    while(42) {
        smatch m;
        
        //Najde dalsi zavorku (ktera v sobe nema zavorku)
        regex_search(expr, m, rgx_brackets);
        string subexpr = m.str();
        
        //Nenelazena dalsi zavorka, konec
        if (subexpr == "")
            break;
        
        //Odstrani znaky '(' a ')'  
        subexpr.erase(0, 1);
        subexpr.erase(subexpr.length()-1, 1);
        
        //Vyresi podvyraz
        double res = solve_expr(subexpr);
        
        //Vysledek prevede na retezec, i se znamenkem +/-
        string str_res = (res > 0) ? '+'+to_string(res) : to_string(res);
        
        //Nahradi zavorku vyresenym podvyrazem
        expr = regex_replace(expr, rgx_brackets, str_res, regex_constants::format_first_only);
    }
    
    //Vyresi vysledny vyraz bez zavorek
    double res = solve_expr(expr);
    
    //Vrati locale do puvodniho stavu
    locale::global(locale(""));
    
    return res;
}
