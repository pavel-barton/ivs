/**
 * @file mathlib.h
 * @brief Matematicka knihovna
 */
 
/**
 * @page rules Matematicka knihovna - formaty operaci
 * @note Tato stranka se venuje vstupum pro funkci solve()
 *
 * ### Uvod ###
 * 
 * Zde najdete popis toho, jake vstupy ocekava matematicka knihovna, konkretne
 * funkce solve(). Ve vyrazech nezalezi na mezerach. 
 * Operace mohou byt ruzne kombinovany, ale museji odpovidat pravidlum.
 * 
 * Napr. `"2! + (2^4 / r4) - 8 * 3 + (3r27 * 19) - 1"` je validni vyraz
 * 
 * ## Pouzite zkratky: ##
 * - D = double hodnota
 *      - muze se jednat jak o cele cislo, tak o desetinne cislo
 *      - platnym oddelovacem desetinneho cisla je desetinna tecka!
 * - N = integer hodnota
 *      - musi se jednat o cele cislo
 * 
 * ## Unarni operace ##
 * - odmocnina (r) : "r D"
 *      - takto zapsane odmocniny jsou chapany jako druhe odmocniny
 *      - napr. "r 42", "r 13.37"
 * - faktorial (!) : "N !"
 *      - N musi byt vetsi nez 0
 *      - napr. "42!", "1337!"
 * 
 *
 * ## Binarni operace ##
 * - scitani (+)   : "D1 + D2"
 *      - napr. "4.2 + 4.2", "-13 + -37"
 * - odcitani (-)  : "D1 - D2"
 *      - napr. "4.2 - 4.2", "-13 - -37"
 * - nasobeni (*)  : "D1 * D2"
 *      - napr. "4.2 * 4.2", "-13 * -37"
 * - deleni (/)    : "D1 / D2"
 *      - napr. "4.2 / 4.2", "-13 / -37"
 * - modulo (%)    : "N % N"
 *      - napr. "4 % 4", "-13 % -37" 
 * 
 * - mocnina (^)   : zapisuje se ve tvaru 'cislo ^ mocnina', tedy "D1 ^ D2"
 *      - napr. "4.2 ^ 4.2", "-13 ^ -37"
 * - odmocnina (r) : zapisuje se ve tvaru 'odmocnina r cislo', tedy "N r D"
 *      - napr. "2 r 4.2" = druha odmocnina ze 4.2
 *      - napr. "3 r 27" = treti odmocnina z 27
 * 
 */
 
#ifndef MATHLIB_H
#define MATHLIB_H

#include <string>
#include <regex>

/**
 * @defgroup mathlib Matematicka knihovna
 * Funkce obsazene v matematicke knihovne
 * @{
 */

/**
 * @defgroup consts Chybove konstanty
 * Konstanty
 * @{
 */
 
///Funkce obdrzela neplatny vyraz
#define ERR_INV "Neplatny vyraz"
///Funkce pro faktorial obdrzela neplatnou hodnotu (desetinne cislo, cislo < 0)
#define ERR_FACT "Neplatny operand pro faktorial"
///Funkce pro modulo obdrzela neplatne hodnoty (desetinne cislo)
#define ERR_MOD "Neplatne operandy pro modulo"
///Funkce pro odmocninu obdrzela neplatne hodnoty (n-ta odmocnina kde n je desetinne)
#define ERR_ROOT "Neplatne operandy pro n-tou odmocninu"
///Deleni nulou
#define ERR_ZERODIV "Deleni nulou neni definovano!"


/**
 * @}
 */

/**
 * @defgroup helpers Pomocne funkce
 * Pomocne funkce matematicke knihovny
 * @{
 */


/**
 * @brief Zjisti zda je v promenne typu double ulozena integer hodnota
 * @param d Testovana double hodnota
 * @return true/false pokud v promenne je/neni integer
 */
bool is_int(double d);


/**
 * @brief Zjisti, zda je cele cislo sude nebo liche
 * @param i Testovane cislo
 * @return true/false pokud je cislo sude/liche 
 */
bool is_even(int i);

/**
 * @brief Vymaze z retezce vsechny mezery
 * @param *str Ukazatel na retezec, se kterym bude funkce pracovat
 * @return Funkce nic nevraci (retezec se predava odkazem)
 */
void remove_spaces(std::string *str);

/**
 * @warning Pocita jen s celymi cisly! (i kdyz je argumentem a navratovou hodnotou double)
 * @brief Vyresi faktorial pro n
 * @param n Operand pro faktorial
 * @return Faktorial pro dane cislo jako double
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskou
 */
double fact(double n);

/**
 * @brief Spocita n-tou odmocninu z cisla x
 * @param n Kolikata odmocnina
 * @param x Cislo ze ktereho pocitame odmocninu
 * @return n-tou odmocninu z x jako double
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskou
 */
double root(int n, double x);

/**
 * @brief Spocita x modulo y (podle Euklidovy definice)
 * @param n Prvni operand
 * @param m Druhy operand
 * @return Vysledek jako integer
 */
int mod(int x, int y);

/**
 * @}
 */

/**
 * @defgroup main Hlavni funkce
 * Funkce primarne urcene pro pouziti mimo matematickou knihovnu
 * @{
 */

/**
 * @brief Vyresi slozity matematicky vyraz s podporovanymi operacemi a zavorkami
 * @warning Podporovane operace zahrnuji +, -, *, /, %, ! (faktorial), ^ (mocnina), r (odmocnina)
 * @param expr Retezec obsahujici vyraz. Vice o vstupnich retezcich @link rules zde @endlink
 * @return Vysledek vyrazu jako double
 * @exception runtime_error Pri chybe, s odpovidajici chybovou hlaskou
 */
double solve(std::string expr);

/**
 * @}
 */

/**
 * @}
 */

#endif
 

