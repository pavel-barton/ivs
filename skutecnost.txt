Prostředky:
    - Původní plán byl vyvíjet v C# pro Windows, z čehož nakonec sešlo
      rozhodli jsme se pro Linux a C++
    - Původně byl jako hosting zamýšlen server ivs.fit.vutbr.cz, kvůli 
      jednoduchosti byl nakonec zvolen bitbucket, se kterým již máme
      zkušenosti

Produkt:
    - Původně byla v plánu velmi jednoduchá kalkulačka jen s binarními
      operacemi, kalkulačka nakonec oproti plánu umí i složité výrazy
      se závorkami

Plan: 
    - Dařilo se nam dodržovat plán projektu co se týče rozdělení práce.
    - Řesení některých úkolů se protáhlo mimo jejich vyhrazený týden.
