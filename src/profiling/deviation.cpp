#include <iostream>
#include <vector>
#include <cmath>
#include <numeric>
#include "libsmath/smath.h"

/*
 * Spocita smerodatnou odchylku
 * Pozn.: V zadani stoji: 
 * "Pomocí funkcí z Vaší matematické knihovny vytvořte program ..."
 * Matematicka knihovna libsmath je stavena na parsovani a pocitani retezcu, 
 * tudiz se k takoveto praci vubec nehodi. Ve snaze dodrzet zadani jsem ji
 * i tak pouzil, program je vsak velmi pomaly
 */
int main() {
    using namespace std;
    
    string sum = "0"; //Pomocny string, uchovava vyraz pro solve
    double num = 0.0; //Pomocna promenna pro nacitani ze vstupu
    double mean = 0.0; //Aritm. prumer
    double res = 0.0; //Vysledek
    
    vector<double> in;
    
    //Nacte cisla ze vstupu
    while (cin >> num) {
        in.push_back(num);
    }
    
    //Spocita aritmeticky prumer
    for (vector<double>::iterator it = in.begin(); it != in.end(); it++) {
        //sum += *it
        sum += "+" + to_string(*it);
    }
    mean = solve(sum) / in.size();
    sum = "0";
    
    //Spocita sumu
    for (vector<double>::iterator it = in.begin(); it != in.end(); it++) {
        //sum += (*it - mean) * (*it - mean)
        sum += " + (" + to_string(*it) + "-" + to_string(mean) + ")^2";
    }
    res = solve(sum);
    
    //Spocita vysledek
    res = root(2, res / (in.size()-1));
    
    cout << "Smerodatna odchylka: " << res << endl;
    return 0;
}
