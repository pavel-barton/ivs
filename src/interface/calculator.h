#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QMainWindow>

namespace Ui {
class Calculator;
}

class Calculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit Calculator(QWidget *parent = 0);
    ~Calculator();

private slots:

    void buttonPress(char num);

    void on_button_num0_clicked();

    void on_button_num1_clicked();

    void on_button_num2_clicked();

    void on_button_num3_clicked();

    void on_button_num4_clicked();

    void on_button_num5_clicked();

    void on_button_num6_clicked();

    void on_button_num7_clicked();

    void on_button_num8_clicked();

    void on_button_num9_clicked();

    void on_button_dot_clicked();

    void on_button_plus_clicked();

    void on_button_minus_clicked();

    void on_button_mul_clicked();

    void on_button_div_clicked();

    void on_button_mod_clicked();

    void on_button_bracket_end_clicked();

    void on_button_bracket_start_clicked();

    void on_button_fact_clicked();

    void on_button_clr_clicked();

    void on_button_res_clicked();

    void on_button_pow_clicked();

    void on_button_root_clicked();

private:
    Ui::Calculator *ui;
    int startAgain;
};

#endif // CALCULATOR_H
