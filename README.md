# IVS repozitář skupiny Muzeum mentálních kuriozit
# Projekt MMKalkulačka
  
## Poznámky k projektu  
  
- Pro správnou funkčnost testů je potřeba provést 'make test-init' (stačí jednou, po prvním naklonování projektu/stažení commitu kde byl přidán gtest)

## Překlad projektu

### Požadavky
- G++ 5.4.0
- QT 5 a vyšší

Překlad se provádí pomocí makefile ve složce src (příkaz 'make', popř. 'make all')

## Instalace projektu
- Instalace se provádí pomocí instalačního souboru ve složce src/install
- Instalaci je nutno spustit z příkazové řádky (ne dvojitým poklepáním)

## Prostředí  
---------
  
Ubuntu 64bit
  
## Autoři
------
  
Muzeum mentálních kuriozit  
- xbarto93 Pavel Bartoň  
- xkalaj01 Jan Kala  
- xsvoja03 Vojtěch Svojanovský   
  
## Licence  
-------
  
Tento program je poskytován včetně zdrojového kódu dle licence GNU GPL v.3