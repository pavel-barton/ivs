#include <algorithm>
#include "calculator.h"
#include "ui_calculator.h"
#include "smath.h"

Calculator::Calculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Calculator)
{
    ui->setupUi(this);
    startAgain=1;
    ui->textEdit_expr->setAlignment(Qt::AlignRight);
}

Calculator::~Calculator()
{
    delete ui;
}

void Calculator::buttonPress(char num){

    if (startAgain==1){
        ui->textEdit_expr->clear();
        startAgain=0;
    }
    ui->textEdit_expr->setText(ui->textEdit_expr->toPlainText()+num);
    ui->textEdit_expr->setAlignment(Qt::AlignRight);
}



void Calculator::on_button_num0_clicked()
{
    buttonPress('0');
}

void Calculator::on_button_num1_clicked()
{
    buttonPress('1');
}

void Calculator::on_button_num2_clicked()
{
    buttonPress('2');
}

void Calculator::on_button_num3_clicked()
{
    buttonPress('3');
}

void Calculator::on_button_num4_clicked()
{
    buttonPress('4');
}

void Calculator::on_button_num5_clicked()
{
    buttonPress('5');
}

void Calculator::on_button_num6_clicked()
{
    buttonPress('6');
}

void Calculator::on_button_num7_clicked()
{
    buttonPress('7');
}

void Calculator::on_button_num8_clicked()
{
    buttonPress('8');
}

void Calculator::on_button_num9_clicked()
{
    buttonPress('9');
}

void Calculator::on_button_dot_clicked()
{
    buttonPress('.');
}

void Calculator::on_button_plus_clicked()
{
    buttonPress('+');
}

void Calculator::on_button_minus_clicked()
{
    buttonPress('-');
}

void Calculator::on_button_mul_clicked()
{
    buttonPress('*');
}

void Calculator::on_button_div_clicked()
{
    buttonPress('/');
}

void Calculator::on_button_mod_clicked()
{
    buttonPress('%');
}

void Calculator::on_button_bracket_end_clicked()
{
    buttonPress(')');
}

void Calculator::on_button_bracket_start_clicked()
{
    buttonPress('(');
}

void Calculator::on_button_fact_clicked()
{
    buttonPress('!');
}

void Calculator::on_button_clr_clicked()
{
    ui->textEdit_expr->clear();
    ui->textEdit_results->clear();
    ui->textEdit_expr->setAlignment(Qt::AlignRight);
}

void Calculator::on_button_res_clicked()
{
    std::string expr = ui->textEdit_expr->toPlainText().toUtf8().constData();
    double result = 0;

    try {
        result = solve(expr);
        ui->textEdit_results->append(ui->textEdit_expr->toPlainText() + " = " + QString::number(result));
        ui->textEdit_expr->setText(QString::number(result));
        ui->textEdit_expr->setAlignment(Qt::AlignRight);
    } catch (std::runtime_error &e) {
        ui->textEdit_results->append("Neplatny vyraz");
        ui->textEdit_expr->setAlignment(Qt::AlignRight);
    }
    startAgain=1;
}

void Calculator::on_button_pow_clicked()
{
    buttonPress('^');
}

void Calculator::on_button_root_clicked()
{
    buttonPress('r');
}
