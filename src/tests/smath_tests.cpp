#include "gtest/gtest.h"
#include "libsmath/smath.h"
#include <ctype.h>

/*
 * ### Knihovni funkce ###
 * double solve(std::string expr);
 * double root(int n, double x);
 * double fact(int n);
 * 
 * ### Makra ###
 * # ASSERT - ukonci test, EXPECT - podminka failne, ale test pokracuje
 * _EQ(vec1, vec2); _NE(vec1, vec2)
 * _TRUE(vec); _FALSE(vec);
 * _NO_THROW(vec); _ANY_THROW(vec);
 * 
 * ### Testy ###
 * TEST (nazev1, nazev2) {
 * 		...
 * 		ASSERT_...
 * 		...
 * }
 * ### GIT ###
 * git status - nahled na zmeny
 * git add tests/smath_tests.cpp - prida soubor do commitu
 * git commit - vytvori commit
 * git push - posle commit(y)
 * 
 */
 
 /**
  * @defgroup tests Testy matematicke knihovny
  * @{
  */
 
/**
 * @brief Test scitani celych kladnych cisel
 */
TEST (Test, obycScitani) {
    using namespace std;
    EXPECT_EQ(solve("40+2"), 42);
    EXPECT_EQ(solve("1+2+6+420+900+4+4"), 1337);
    EXPECT_EQ(solve("123456789+987654321"), 1111111110);
}

/**
 *@brief Test scitani celych cisel se znamenkem
 */
 TEST (Test, znamScitani) {
	EXPECT_EQ(solve("-78+147"), 69);
	EXPECT_EQ(solve("-2674+1337"), -1337);
	EXPECT_EQ(solve("-0+0"), 0);
}

/**
 * @brief Test scitani desetinnych cisel se znamenkem 
 */
TEST (Test, znamScitaniDesCisel) {
	EXPECT_EQ(solve("6.9+4.2"),11.1);
	EXPECT_EQ(solve("-0.123456+0.001"), -0.122456);
	EXPECT_EQ(solve("7+6.37"), 13.37);
}

/**
 * @brief Test odcitani celych kladnych cisel
 * */
TEST(Test, obycOdcitani) {
	EXPECT_EQ(solve("9-6"), 3);
	EXPECT_EQ(solve("100-10-10-40-3"), 37);
	EXPECT_EQ(solve("987654321-98765432"), 888888889);
}

/**
 * @brief Test odcitani celych cisel se znamenkem
 * */
TEST(Test, znamOdcitani) {
	EXPECT_EQ(solve("42-69"), -27);
	EXPECT_EQ(solve("-3--3"), 0);
	EXPECT_EQ(solve("1337-69-420-911--88"), 25);
}

/**
 * @brief Test odcitani desetinnych cisel se znamenkem
 * */
TEST(Test, znamOdcitaniDesCisel) {
	EXPECT_EQ(solve("6.9-9.11"), -2.21);
	EXPECT_EQ(solve("135.135-135-0.135-135.135"), -135.135);
	EXPECT_EQ(solve("-0.05-911911911"), -911911911.05);
}

/**
 * @brief Test nasobeni celych kladnych cisel
 * */
TEST(Test, obycNasobeni) {
	EXPECT_EQ(solve("25*25"), 625);
	EXPECT_EQ(solve("69*0"), 0);
	EXPECT_EQ(solve("9*111111111*1*1"), 999999999);
}

/**
 * @brief Test nasobeni celych cisel se znamenkem
 * */
TEST(Test, znamNasobeni) {
	EXPECT_EQ(solve("13*-5"), -65);
	EXPECT_EQ(solve("-420*-50"), 21000);
	EXPECT_EQ(solve("9*-9*9*-9*9*-9*9*-9"), 43046721);
}

/**
 * @brief Test nasobeni desetinnych cisel se znamenkem
 * */
TEST(Test, znamNasobeniDesCisel) {
	EXPECT_EQ(solve("42.420*6.6913*-37"), -10502.263002);
	EXPECT_EQ(solve("100.100*-0.01"), -1.001);
	EXPECT_EQ(solve("13.5*-10*0.1"), -13.5);
}

/**
 * @brief Test deleni celych kladnych cisel
 * */
TEST(Test, obycDeleni) {
	EXPECT_EQ(solve("135/5"), 27);
	EXPECT_EQ(solve("25/50"), 0.5);
	EXPECT_EQ(solve("100000/10/10/10"), 100);
}

/**
 * @brief Test deleni celych cisel se znamenkem
 * */
TEST(Test, znamDeleni) {
	EXPECT_EQ(solve("420/-10"), -42);
	EXPECT_EQ(solve("-999/111"), -9);
	EXPECT_EQ(solve("42000/4200/-10"), -1);
}

/**
 * @brief Test deleni desetinnych cisel se znamenkem
 * */
TEST(Test, znamDeleniDesCisel) {
	EXPECT_EQ(solve("121.121/11"), 11.011);
	EXPECT_EQ(solve("-0.5/2"), -0.250);
	EXPECT_EQ(solve("-10/10/-1"), 1);
}

/**
 * @brief Test osetreni deleni nulou
 * */
TEST(Test, DeleniNulou) {
	ASSERT_ANY_THROW(solve("1337/0"));
}

/**
 * @brief Testy modulo - cela kladna cisla
 * */
TEST(Test, obycModulo) {
	EXPECT_EQ(solve("3%2"), 1);
	EXPECT_EQ(solve("150%5"), 0);
	EXPECT_EQ(solve("5%6"), 5);
}

/**
 * @brief Testy modulo - cela cisla se znamenkem
 * */
TEST(Test, znamModulo) {
	EXPECT_EQ(solve("5%(-4)"), -3);
	EXPECT_EQ(solve("-9%4"), 3);
	EXPECT_EQ(solve("-5%-6"), -5);
}

/**
 * @brief Test umocnovani celych kladnych cisel
 * */
TEST(Test, obycUmocneni) {
	EXPECT_EQ(solve("4^2"), 16);
	EXPECT_EQ(solve("5^6"), 15625);
	EXPECT_EQ(solve("69^0"), 1);
}

/**
 * @brief Test umocnovani celych cisel se znamenkem
 * */
TEST(Test, znamUmocneni) {
	EXPECT_EQ(solve("-3^7"), -2187);
	EXPECT_EQ(solve("4^-2"), 0.0625);
	EXPECT_EQ(solve("-1^-2"), 1);
}

/**
 * @brief Test umocnovani desetinnych cisel se znamenkem
 * */
TEST(Test, znamUmocneniDesCisel) {
	EXPECT_DOUBLE_EQ(solve("6.9^3"), 328.509);
	EXPECT_DOUBLE_EQ(solve("4^3.5"), 128);
	EXPECT_DOUBLE_EQ(solve("-5^2"), 25);
}

/**
 * @brief Test odmocnovani celych cisel
 * */
TEST(Test, obycOdmocnina) {
	EXPECT_DOUBLE_EQ(root(2, 49), 7);
	EXPECT_DOUBLE_EQ(root(4, 81), 3);
	EXPECT_DOUBLE_EQ(root(2, 625), 25);
	EXPECT_DOUBLE_EQ(solve("2r49"), 7);
	EXPECT_DOUBLE_EQ(solve("4r81"), 3);
	EXPECT_DOUBLE_EQ(solve("2r625"), 25);
}

/**
 * @brief Test odmocnovani desetinnych cisel
 * */
TEST(Test, OdmocninaDesCisel) {
	EXPECT_NEAR(root(2, 625.4), 25.0079987204, 25.0079987204 * 10e-7);
}

/**
 * @brief Test vypoctu matematickych vyrazu
 * */
TEST(Test, matVyrazy) {
	EXPECT_DOUBLE_EQ(solve("420-3*1337+911/1"), -2680);
	EXPECT_DOUBLE_EQ(solve("135.2*8.1-3+123.321*-2+13/8"), 847.103);
	EXPECT_DOUBLE_EQ(solve("3r27*2+9*15+20+8^2"), 225);
}

/**
 * @brief Test vypoctu matematickych vyrazu se zavorkami
 * */
TEST(Test, matVyrazyZavorky) {
	EXPECT_EQ(solve("(1+1)*2"), 4);
	EXPECT_EQ(solve("(3*(9-8))^2-5"), 4);
	EXPECT_EQ(solve("(8/(2^1))^(1+1)"), 16);
	EXPECT_EQ(solve("(3+2)!*2+180"), 420);
}

/**
 * @brief Test vypoctu faktorialu
 * */
TEST(Test, faktorial) {
	EXPECT_DOUBLE_EQ(fact(5), 120);
	EXPECT_DOUBLE_EQ(solve("5!"), 120);
	EXPECT_NEAR(fact(69), 1.7112245e+98, 1.7112245e+98 * 10e-7);
	EXPECT_NEAR(solve("69!"), 1.7112245e+98, 1.7112245e+98 * 10e-7 );
}

/**
 * @brief Ruzne testy
 * */
TEST(Test, Ruzne) {
	EXPECT_EQ(fact(0), 1);
	EXPECT_ANY_THROW(solve("3*1+)/2"));
	EXPECT_ANY_THROW(fact(-5));
	EXPECT_ANY_THROW(root(2, -2));
	EXPECT_ANY_THROW(solve("1+"));
	EXPECT_ANY_THROW(solve("17+3/"));
	EXPECT_ANY_THROW(solve("69+/2"));
	EXPECT_ANY_THROW(solve("3---8"));
	EXPECT_EQ(solve("3,14+3"), 6.14);
}

/**
 * @brief Testy neplatnych vstupu
 * */
TEST(Test, neplatneVstupy) {
	EXPECT_ANY_THROW(solve(""));
	EXPECT_ANY_THROW(solve("+"));
	EXPECT_ANY_THROW(solve("retezec?!"));
	EXPECT_ANY_THROW(solve("x+2=420"));
	EXPECT_ANY_THROW(solve("r!"));
}
/**
 * @}
 */
